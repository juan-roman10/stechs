<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Stechs | Panel de Control</title>

  <link href="views/img/plantilla/logoF.png" rel="icon">

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
  <!-- Font Awesome -->
  <link rel="stylesheet" href="views/plugins/fontawesome-free/css/all.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="views/dist/css/adminlte.min.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


  <!-- SweetAlert -->
  <link rel="stylesheet" type="text/css" href="views/css/sweetalert2.min.css">



  <!-- jQuery -->
  <script src="views/plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI -->
<script src="views/plugins/jquery-ui/jquery-ui.min.js"></script>

  <!-- Sweet Alert JS -->
  <script src="views/js/sweetalert2.all.min.js"></script>


</head>


<?php


  echo '<body class="hold-transition sidebar-mini skin-blue">

  <div class="wrapper">';

    // Header
    include "modules/header.php";

    // Menú Lateral
    include "modules/menuLateral.php";

    // Contenido
    
    if (isset($_GET['ruta'])) {

      if ($_GET['ruta'] == 'modems' ||
          $_GET['ruta'] == 'json') {
       
          include "modules/".$_GET['ruta'].".php";
         
      }

    } else {

      include "modules/modems.php";

    }

    // Footer
    
    include "modules/footer.php";


  echo '</div>';

?>


<!-- Bootstrap 4 -->
<script src="views/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- AdminLTE App -->
<script src="views/dist/js/adminlte.min.js"></script>

<script src="views/js/modem.js"></script>
<script src="views/js/json.js"></script>

</body>
</html>

$(".buscadorModem").change(function(){

	$(".modems .tituloResultado").remove();
	$(".tablaModems .resultado").remove();

	var vsi_vendor = $(this).val();

	$.ajax({

		url: "http://localhost/api.stechs/modems/"+vsi_vendor,
		method: "GET",
		cache: false,
		contentType: false,
		processData: false,
	    dataType: "json",
		success:function(respuesta){

			$(".modems").prepend('<p class="tituloResultado">Resultado de la busqueda: '+vsi_vendor+' - '+respuesta.length+' resultados</p>')

			if (respuesta != "") {
				for (var i = 0; i < respuesta.length; i++) {

					$(".tablaModems").append('<tr class="resultado"><td>'+respuesta[i]["modem_macaddr"]+'</td><td>'+respuesta[i]["ipaddr"]+'</td><td>'+respuesta[i]["vsi_model"]+'</td><td>'+respuesta[i]["vsi_swver"]+'</td><td><button class="btn btn-success btnAgregarJson" vendor='+respuesta[i]["vsi_vendor"]+' name='+respuesta[i]["vsi_model"]+' soft="'+respuesta[i]["vsi_swver"]+'"><i class="fas fa-plus-circle"></i></button></td></tr>')

				} 

			} else {

				Swal.fire({
				  type: "error",
				  title: "No hay resultados",
				  text: "El vendor "+vsi_vendor+" no existe",
				  showCancelButton: false,
				  confirmButtonColor: "#3085d6",
				  showConfirmButton: true,
				  confirmButtonText: "Cerrar"
				  }).then(function(result){
					if (result.value) {

					window.location = "modems";

					}
				})

			}

		}

	})

})

/*=============================================
BUSCADOR CON ENTER
=============================================*/
$(document).ready(function(){

    $(".buscadorModem").keypress(function(e) {

        var code = (e.keyCode ? e.keyCode : e.which);

        if(code == 13 && $(".buscadorModem").val() != ""){

        	e.preventDefault();

            $('.buscadorModem').trigger('change');

        }

    });

});


/*=============================================
GUARDAR JSON
=============================================*/
$(".tablaModems").on("click", ".btnAgregarJson", function(){
	var vendor = $(this).attr("vendor");
	var name = $(this).attr("name");
	var soft = $(this).attr("soft");

	var datos = new FormData();
	datos.append("vendor", vendor);
	datos.append("name", name);
	datos.append("soft", soft);

	$.ajax({

		url: "http://localhost/api.stechs/json",
		method: "POST",
		data: datos, 
		cache: false,
		contentType: false,
		processData: false,
	    dataType: "json",
		success:function(respuesta){

			if (respuesta) {

				Swal.fire({
				  type: "success",
				  title: "El módem se ha agregado al archivo JSON",
				  text: "El modelo "+name+" ha sido agregado",
				  showCancelButton: false,
				  confirmButtonColor: "#3085d6",
				  showConfirmButton: true,
				  confirmButtonText: "Cerrar"
				  }).then(function(result){
					if (result.value) {

					window.location = "json";

					}
				})

			}

		}

	})

})




<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">

      <div class="container-fluid">

        <div class="row mb-2">

          <div class="col-sm-6">

            <h1>Modems</h1>

          </div>

          <div class="col-sm-6">

            <ol class="breadcrumb float-sm-right">

              <li class="breadcrumb-item active">Modems</li>

            </ol>

          </div>

        </div>

      </div><!-- /.container-fluid -->

    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default card -->
      <div class="card card-outline card-primary">
        
          <div class="card-header with-border">

              <form method="post" style="width: 50%">

                <div class="input-group mb-3">

                  <input type="text" class="form-control buscadorModem" placeholder="Introduzca el vendor" aria-label="Recipient's username" aria-describedby="button-addon2">

                  <div class="input-group-append">

                    <button class="btn btn-primary botonBuscador" type="button" id="button-addon2">Buscar</button>

                  </div>

                </div>

              </form>

          </div>

          <div class="card-body modems">

            <table class="table table-striped dt-responsive table-bordered tablaModems" width="100%">
              
              <thead>
                
                <tr>
                  
                  <th>MAC Address</th>

                  <th>IP</th>

                  <th>Modelo</th>

                  <th>Versión SW</th>

                  <th style="width: 20px">Agregar</th>

                </tr>

              </thead>


            </table>

          </div>

      </div>
      <!-- /.card -->
 
    </section>
    <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->

 
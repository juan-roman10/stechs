<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">

      <div class="container-fluid">

        <div class="row mb-2">

          <div class="col-sm-6">

            <h1>JSON</h1>

          </div>

          <div class="col-sm-6">

            <ol class="breadcrumb float-sm-right">

              <li class="breadcrumb-item active">Json</li>

            </ol>

          </div>

        </div>

      </div><!-- /.container-fluid -->

    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default card -->
      <div class="card card-outline card-primary">
        
          <div class="card-body">

            <table class="table table-striped dt-responsive table-bordered tablaJson" width="100%">
              
              <thead>
                
                <tr>
                  
                  <th>Vendor</th>

                  <th>Modelo</th>

                  <th>Versión</th>

                </tr>

              </thead>


            </table>

          </div>

      </div>
      <!-- /.card -->
 
    </section>
    <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->

 
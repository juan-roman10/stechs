  <!-- Sidebar Menu -->
  <nav class="mt-2">

    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

      <li class="nav-item">

        <a href="modems" class="nav-link">

          <i class="nav-icon fas fa-ethernet"></i>

          <p>Modems</p>

        </a>

      </li>

      <li class="nav-item">

        <a href="json" class="nav-link">

          <i class="nav-icon fas fa-file-code"></i>

          <p>JSON</p>

        </a>

      </li>

    </ul>

  </nav>
  <!-- /.sidebar-menu -->


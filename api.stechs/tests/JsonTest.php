<?php 

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class JsonTest extends TestCase 
{

	private $client;

	protected function setup(): void
	{

		$this->client = new Client([
			'base_uri' => 'http://localhost/api.stechs/',
			'http_errors' => false
		]);

	}

	// Testeamos que cargue el json
	public function testGetAllJsonTrue()
	{

		$res = $this->client->request('GET', 'json');

		$body = json_decode($res->getBody(), true);

		$this->assertEquals(200, $res->getStatusCode());

		$this->assertGreaterThan(0, count($body));

	}

	// Testeamos error 404 con rutas incorrectas
	public function testRouteNotExist()
	{

		$res = $this->client->request('GET', 'json/34');

		$res1 = $this->client->request('GET', 'jsons');

		$res2 = $this->client->request('GET', 'json/3ijfosd');

		$this->assertEquals(404, $res->getStatusCode());

		$this->assertEquals(404, $res1->getStatusCode());

		$this->assertEquals(404, $res2->getStatusCode());

	}

	// Testeamos error 404 con metodo incorrecto
	public function testMethodIncorrect()
	{

		$res = $this->client->request('PUT', 'json');

		$this->assertEquals(404, $res->getStatusCode());

	}

	// Testeamos error 500 con POST sin parametros
	public function testPostNoParameters()
	{

		$res = $this->client->request('POST', 'json');

		$this->assertEquals(500, $res->getStatusCode());

	}


}
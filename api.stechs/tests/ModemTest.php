<?php 

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class ModemTest extends TestCase 
{

	private $client;

	protected function setup(): void
	{

		$this->client = new Client([
			'base_uri' => 'http://localhost/api.stechs/',
			'http_errors' => false
		]);

	}

	// Testeamos que llegue la respuesta con un vendor correcto
	public function testGetModemTrue()
	{

		$res = $this->client->request('GET', 'modems/cisco');

		$body = json_decode($res->getBody(), true);

		$this->assertEquals(200, $res->getStatusCode());

		$this->assertGreaterThan(0, count($body));

	}

	// Testeamos que llegue la respuesta vacia con un vendor incorrecto
	public function testGetModemFalse()
	{

		$res = $this->client->request('GET', 'modems/tplink');

		$body = json_decode($res->getBody(), true);

		$this->assertEquals(200, $res->getStatusCode());

		$this->assertEquals(0, count($body));

	}

	// Testeamos error 404 con rutas incorrectas
	public function testRouteNotExist()
	{

		$res = $this->client->request('GET', 'modems/cisco/34');

		$res1 = $this->client->request('GET', 'modem/cisco');

		$res2 = $this->client->request('GET', 'modems/cisco/3ijfosd');

		$this->assertEquals(404, $res->getStatusCode());

		$this->assertEquals(404, $res1->getStatusCode());

		$this->assertEquals(404, $res2->getStatusCode());

	}

	// Testeamos error 404 con metodo incorrecto
	public function testMethodIncorrect()
	{

		$res = $this->client->request('POST', 'modems/cisco');

		$this->assertEquals(404, $res->getStatusCode());

	}

	// Testeamos el filtrado correcto de vendors en la respuesta
	public function testVendorResponseTrue()
	{

		$res = $this->client->request('GET', 'modems/cisco');

		$body = json_decode($res->getBody(), true);

		foreach ($body as $key => $value) 
		{

			$this->assertEquals("Cisco", $value["vsi_vendor"]);
			
		}

	}

}
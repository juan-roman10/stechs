<?php

use DI\Container;
use Slim\Factory\AppFactory;

return function ()
{

    $container = new Container();

    $settings = require 'settings.php';

    // Settings
    $container->set('settings', function () use ($settings) 
    {

        return $settings;

    });

    // Monolog
    $container->set('logger', function () use ($settings) 
    {

        $logger = new \Monolog\Logger($settings['log']['channel']);

	    $file_handler = new \Monolog\Handler\StreamHandler($settings['log']['path'].date('Ymd').'.log');

	    $logger->pushHandler($file_handler);

	    return $logger;

    });

    AppFactory::setContainer($container);

};
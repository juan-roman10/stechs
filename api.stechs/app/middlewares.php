<?php
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\App;
use Slim\Psr7\Response;

return function (App $app)
{

    $container = $app->getContainer();
    $settings = $container->get('settings');

    // Add Routing Middleware
    $app->addRoutingMiddleware();

    //Default
    $app->addBodyParsingMiddleware();

    // Error Middleware
    $errorMiddleware = $app->addErrorMiddleware($settings['displayErrors'], false, false);

    if (!$settings['displayErrors']) 
    {

        $errorMiddleware->setDefaultErrorHandler(function (
            ServerRequestInterface $request,
            Throwable $exception,
            bool $displayErrorDetails,
            bool $logErrors,
            bool $logErrorDetails
        ) use ($app, $container) 
        {

            $status_code = $exception->getCode() === 0 ? 500 : $exception->getCode();

            $payload = [
                'error' => 'Ocurrio un error personalizado',
                'status_code' => $status_code
            ];

            if ($status_code >= 500 && $status_code < 600) 
            {

                $container->get('logger')->error($exception->getMessage().' - '.'File: '.$exception->getFile().'('.$exception->getLine().')');

            }

            $response = $app->getResponseFactory()->createResponse();

            $response->getBody()->write(json_encode($payload, JSON_UNESCAPED_UNICODE));

            return $response->withHeader('Content-Type', 'application/json')
                            ->withStatus($status_code);;

        });

    }

    // CORS Middleware
    $app->add(function ($request, $handler) 
    {

        $response = $handler->handle($request);

        return $response
                ->withHeader('Access-Control-Allow-Origin', '*')
                ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');

    });
    
};
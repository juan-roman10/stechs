<?php 

use Illuminate\Database\Capsule\Manager as Capsule;

return function($connection) {

    // Instanciamos
    $capsule = new Capsule;

    //Creamos el string de conexion
    $capsule->addConnection($connection);

    //Lo seteo como global
    $capsule->setAsGlobal();

    //Inicializamos
    $capsule->bootEloquent();

};
<?php

use App\Controllers\JsonController;
use App\Controllers\ModemController;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Exception\HttpNotFoundException;
use Slim\Routing\RouteCollectorProxy;

return function(App $app)
{

    $container = $app->getContainer();
    $settings = $container->get('settings');

    // CORS
    $app->options($settings['basePath'].'{routes:.+}', function ($request, $response, $args) 
    {

        return $response;

    });

    $app->get($settings['basePath'], function (Request $request, Response $response, $args) 
    {

        $response->getBody()->write("Running...");
        return $response;

    });

    //Modem Controller
    $app->group($settings['basePath'] . 'modems', function (RouteCollectorProxy $group)
    {
        
        $group->get('', ModemController::class .':getAll');
        $group->get('/{vsi_vendor}', ModemController::class .':get');
    
    });

    //Json Controller
    $app->group($settings['basePath'] . 'json', function (RouteCollectorProxy $group)
    {
        
        $group->get('', JsonController::class .':getAll');
        $group->post('', JsonController::class .':create');
    
    });
    

    $app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], $settings['basePath'].'{routes:.+}', function ($request, $response) 
    {

        throw new HttpNotFoundException($request);

    });

};


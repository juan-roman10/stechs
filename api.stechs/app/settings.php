<?php

return [
    'basePath' => '/api.stechs/',
    'displayErrors' => false, 
    'log' => [
        'channel' => 'stechs',
        'path' => '../logs/'
    ],
    'db' => [
        'driver' => 'mysql',
        'host' => 'localhost',
        'database' => 'modems',
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8',
        'collation' => 'utf8_general_ci',
        'prefix' => '',
    ],
];
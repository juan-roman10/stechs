<?php
use Slim\Factory\AppFactory;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

//Capturamos todos los errores y los transformamos en excepciones
function exception_error_handler($severidad, $mensaje, $fichero, $linea)
{

	if (!(error_reporting() & $severidad)) 
	{

		return;

	}

	throw new ErrorException($mensaje, 0, $severidad, $fichero, $linea);
	
}

set_error_handler("exception_error_handler");


require __DIR__ . '/../vendor/autoload.php';

//Configuramos las dependencias
$dependenciesConfig = require __DIR__ . '/../app/dependencies.php';
$dependenciesConfig();

$app = AppFactory::create();

//Configuramos los middlewares
$middlewaresConfig = require __DIR__ . '/../app/middlewares.php';
$middlewares = $middlewaresConfig($app);

//Configuramos la BD
$dbConfig = require __DIR__ . '/../app/database.php';
$dbConfig($app->getContainer()->get('settings')['db']);

//Configuramos las rutas
$routeConfig = require __DIR__ . '/../app/routes.php';
$routeConfig($app, $middlewares);


$app->run();
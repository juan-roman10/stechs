<?php

namespace App\Controllers;

use App\Services\JsonService;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class JsonController {

    protected $container;
    private $_jsonService;
    private $_logger;

    public function __construct(ContainerInterface $container)
    { 

        $this->container = $container;
        $this->_jsonService = new JsonService();
        $this->_logger = $container->get('logger');

    }

    /*----------  Traigo el JSON  ----------*/
    public function getAll(Request $request, Response $response, $args) 
    {
        /*----------  Traigo el JSON  ----------*/
        $json = file_get_contents('..\\src\\json\\models.json');
        
        $response->getBody()->write($json);

        return $response->withHeader('Content-Type', 'application/json')
                        ->withStatus(200);

    }

    /*----------  Agrego el modelo al JSON  ----------*/
    public function create(Request $request, Response $response, $args) 
    {

        $settings = $this->container->get('settings');
        
        /*----------  Recupero los args y convierto a array  ----------*/
        $body = (object) $request->getParsedBody();

        $bodyArray = json_decode( json_encode( $body ), true );

        $this->_logger->info('add "'.$bodyArray["name"].'" started');

        /*----------  Traigo el JSON  ----------*/
        $json = file_get_contents('..\\src\\json\\models.json');

        $modelsJson = json_decode($json, true);

        /*----------  Creo el nuevo registro en el JSON  ----------*/
        $newJson = array("vendor" => $bodyArray["vendor"], "name" => $bodyArray["name"], "soft" => $bodyArray["soft"]);

        array_push($modelsJson["models"], $newJson);

        $modelsJson_string = json_encode($modelsJson); 
        
        /*----------  Guardo el JSON  ----------*/
        $file = '..\\src\\json\\models.json';
        
        file_put_contents($file, $modelsJson_string); 

        $this->_logger->info('New "models.json" generated');
        
        $response->getBody()->write(json_encode($body));

        return $response->withHeader('Content-Type', 'application/json')
                        ->withHeader('Location', $settings['basePath']. 'json')
                        ->withStatus(201);

    }

}
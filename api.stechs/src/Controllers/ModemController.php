<?php

namespace App\Controllers;

use App\Services\ModemService;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ModemController {

    protected $container;
    private $_modemService;
    private $_logger;

    public function __construct(ContainerInterface $container){  

        $this->container = $container;
        $this->_modemService = new ModemService($container);
        $this->_logger = $container->get('logger');

    }

    /*----------  Traigo todos los modems  ----------*/
    public function getAll(Request $request, Response $response, $args) 
    {

        $result = $this->_modemService->getAll();
        
        $response->getBody()->write($result->toJson());

        return $response->withHeader('Content-Type', 'application/json')
                        ->withStatus(200);

    }

    /*----------  Traigo modems según vendor  ----------*/
    public function get(Request $request, Response $response, $args) 
    {

        $result = $this->_modemService->get($args['vsi_vendor']);

        /*----------  Armo el JSON de la respuesta  ----------*/
        $array1 = $result->toJson();
        $array1 = json_decode($array1, true);

        /*----------  Traigo el JSON de models  ----------*/
        $json = file_get_contents('..\\src\\json\\models.json');
        $json1 = json_decode($json, true);
        $array2 = $json1["models"];


        /*----------  Comparo los JSON  ----------*/
        $diff = [];

        foreach ($array1 as $key => $value) 
        {
            
            foreach ($array2 as $key1 => $value1) 
            {

                if ($value1["name"] === $value["vsi_model"])
                {

                    continue 2;

                }

            }

            array_push($diff, $value);

        }

        $resultado = json_encode($diff);

        /*----------  Armo el cuerpo de la respuesta  ----------*/
        $response->getBody()->write($resultado);

        if (count($array1) != 0) 
        {

            $this->_logger->info('ModemService get "'.$args["vsi_vendor"].'" ended');

            return $response->withHeader('Content-Type', 'application/json')
                            ->withStatus(200);

        } else {
            
            $this->_logger->error('Vendor "'.$args["vsi_vendor"].'" not exist');

            return $response->withHeader('Content-Type', 'application/json')
                            ->withStatus(200);
        }

    }

}
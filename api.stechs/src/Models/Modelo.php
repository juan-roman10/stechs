<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Modelo extends Model
{

	protected $vsi_vendor;
	protected $ipaddr;
	protected $vsi_swver;
	private $vsi_hwver;

    /*----------  Getter y Setter Hardware version  ----------*/
	public function getVsiHwver()
    {

        return $this->vsi_hwver;

    }

    public function setVsiHwver($vsi_hwver)
    {

        $this->vsi_hwver = $vsi_hwver;

        return $this;

    }

}

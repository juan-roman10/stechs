<?php 

namespace App\Models;

use App\Models\Modelo;
use Illuminate\Database\Eloquent\Model;

class Modem extends Modelo
{

    public $timestamps = false;

	private $vsi_model;
	private $modem_macaddr;

	protected $table = 'docsis_update';

	/*----------  Getter y Setter vendor  ----------*/
    public function getVsiVendor()
    {
        return $this->vsi_vendor;
    }

    public function setVsiVendor($vsi_vendor)
    {
        $this->vsi_vendor = $vsi_vendor;

        return $this;
    }

    /*----------  Getter y Setter ipaddr ----------*/
    public function getIpaddr()
    {
        return $this->ipaddr;
    }

    public function setIpaddr($ipaddr)
    {
        $this->ipaddr = $ipaddr;

        return $this;
    }

    /*----------  Getter y Setter version soft  ----------*/
    public function getVsiSwver()
    {
        return $this->vsi_swver;
    }

    public function setVsiSwver($vsi_swver)
    {
        $this->vsi_swver = $vsi_swver;

        return $this;
    }
    
    /*----------  Getter y Setter model  ----------*/
    public function getVsi_model()
    {
        return $this->vsi_model;
    }

    public function setVsi_model($vsi_model)
    {
        $this->vsi_model = $vsi_model;

        return $this;
    }

    /*----------  Getter y Setter mac address  ----------*/
    public function getModem_macaddr()
    {
        return $this->modem_macaddr;
    }

    public function setModem_macaddr($modem_macaddr)
    {
        $this->modem_macaddr = $modem_macaddr;

        return $this;
    }
    
}
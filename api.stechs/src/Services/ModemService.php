<?php

namespace App\Services;

use App\Models\Modem;
use Psr\Container\ContainerInterface;

class ModemService 
{

	private $_logger;

    public function __construct(ContainerInterface $container)
    {  

        $this->_logger = $container->get('logger');

    }

    /*----------  Traigo todos los modems  ----------*/
    public function getAll()
    {

        return Modem::select('vsi_vendor', 'modem_macaddr', 'ipaddr', 'vsi_model', 'vsi_swver')->get();
         
    }


    /*----------  Traigo modems según vendor  ----------*/
    public function get(string $vendor)
    {

    	$this->_logger->info('ModemService get "'.$vendor.'" started');

        return Modem::select('vsi_vendor', 'modem_macaddr', 'ipaddr', 'vsi_model', 'vsi_swver')->where('vsi_vendor', 'LIKE', "%$vendor%")->get();
      
    }

}
